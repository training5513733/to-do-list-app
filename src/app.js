const Program = (function () {
    class TaskItem {
        constructor(taskName = "") {
            this.taskName = taskName;
            this.renderHTML();
        }

        renderHTML() {
            const tasks = document.getElementById("tasks");

            if (tasks.childElementCount >= 4) {
                tasks.classList.add("scrollable");
            } else if (tasks.childElementCount < 4) {
                tasks.classList.remove("scrollable");
            }

            const tempContainer = document.createElement("article");

            tempContainer.innerHTML = `
                <article class="tasks__item">
                    <span class="tasks__item__title__tooltip">Click to be done</span>
                    <span class="tasks__item__title">${this.taskName}</span>
                    <button class="btn tasks__item__remove-btn">remove</button>
                </article>
            `;

            const taskElement = tempContainer.firstElementChild;
            const titleTask = taskElement.querySelector(".tasks__item__title");
            const removeTaskButton = taskElement.querySelector(".tasks__item__remove-btn");

            this.#makeAsDone(titleTask);
            this.#remove(taskElement, removeTaskButton);

            tasks.appendChild(taskElement);
        }

        #makeAsDone(taskTitle) {
            taskTitle.addEventListener("click", () => taskTitle.classList.toggle("done"));
        }

        #remove(taskEl, removeTaskButton) {
            removeTaskButton.addEventListener("click", () => taskEl.remove());
        }
    }

    class App {
        static init() {
            const addTaskInputButton = document.getElementById("add-task-btn");
            const addTaskInputValue = document.getElementById("add-task-input");

            addTaskInputButton.addEventListener("click", (event) => {
                event.preventDefault();

                if (addTaskInputValue.value.trim() !== "") {
                    new TaskItem(addTaskInputValue.value);
                    addTaskInputValue.value = "";
                }
            });
        }
    }

    return {
        start() {
            App.init();
        }
    };
})();

Program.start();